export const SEARCH_UPDATE = 'SEARCH_UPDATE'

export function updateSearch (searchPhrase) {
  return {
    type: SEARCH_UPDATE,
    payload: {searchPhrase}
  }
}
