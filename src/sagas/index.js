import {call, put, takeLatest} from 'redux-saga/effects'

import {
    FETCH_ROWS,
    CREATE_ROWS_HANDLER,
    CREATE_ROWS_SUCCESS,
    CREATE_ROWS_ERROR,
    READ_ROWS_HANDLER,
    READ_ROWS_SUCCESS,
    READ_ROWS_ERROR,
    DELETE_ROWS_HANDLER,
    DELETE_ROWS_SUCCESS,
    DELETE_ROWS_ERROR
} from '../actions/Table'
import {
    UPDATE_ROWS_HANDLER,
    UPDATE_ROWS_SUCCESS,
    UPDATE_ROWS_ERROR
} from '../actions/Edit'
import api from '../api'

function* rowsHandler(handler, action) {
    yield put({type: FETCH_ROWS})
    let errorAction;
    try {
        switch (handler) {
            case CREATE_ROWS_HANDLER:
                errorAction = CREATE_ROWS_ERROR
                let rowId = yield call(api.create, action.payload)
                rowId = yield rowId.json()
                yield put({type: CREATE_ROWS_SUCCESS, row: {...action.payload, _id: rowId}})
                break;
            case READ_ROWS_HANDLER:
                errorAction = READ_ROWS_ERROR
                let rows = yield call(api.read)
                rows = yield rows.json()
                yield put({type: READ_ROWS_SUCCESS, fetchedRows: rows})
                break;
            case UPDATE_ROWS_HANDLER:
                errorAction = UPDATE_ROWS_ERROR
                yield call(api.update, action.payload, action.payload._id)
                yield put({type: UPDATE_ROWS_SUCCESS, row: action.payload})
                break;
            default:
                errorAction = DELETE_ROWS_ERROR
                yield call(api.delete, action.payload)
                yield put({type: DELETE_ROWS_SUCCESS, id: action.payload})
                break;
        }
    } catch (e) {
        yield put({type: errorAction, err: e})
    }
}

export default function* () {
    yield takeLatest(CREATE_ROWS_HANDLER, rowsHandler, CREATE_ROWS_HANDLER)
    yield takeLatest(READ_ROWS_HANDLER, rowsHandler, READ_ROWS_HANDLER)
    yield takeLatest(UPDATE_ROWS_HANDLER, rowsHandler, UPDATE_ROWS_HANDLER)
    yield takeLatest(DELETE_ROWS_HANDLER, rowsHandler, DELETE_ROWS_HANDLER)
}
