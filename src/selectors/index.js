import {createSelector} from 'reselect'

const getRows = (state) => state.table.rows
const getSearchPhrase = (state) => state.search.searchPhrase

export const getSearchFilteredRows = createSelector(
  [getRows, getSearchPhrase],
  (rows, searchPhrase) => (rows && rows.length) ? rows.filter(row =>
    !searchPhrase ||
          row.name.indexOf(searchPhrase) !== -1 ||
          row.tel.indexOf(searchPhrase) !== -1 ||
          row.email.indexOf(searchPhrase) !== -1
  ) : []
)
