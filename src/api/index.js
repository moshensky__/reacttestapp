/**
 * Api to work with persist storage (localstorage)
 *
 * UPD: update from storage to node API
 */

export default {
    // setRows: (rows) => new Promise(resolve => {
    //     setTimeout(() => {
    //         window.localStorage.setItem('rows', JSON.stringify(rows))
    //         resolve()
    //     }, 1000)
    // }),
    //
    // getRows: () => new Promise(resolve => {
    //     setTimeout(() => {
    //         resolve(window.localStorage.getItem('rows'))
    //     }, 1000)
    // }),

    create: (row) => {
        return fetch('//localhost:8082/phonebook/create', {
            method: 'POST',
            body: JSON.stringify(row)
        });
    },

    read: () => {
        return fetch('//localhost:8082/phonebook/read');
    },

    update: (row, id) => {
        return fetch('//localhost:8082/phonebook/update/' + id, {
            method: 'POST',
            body: JSON.stringify(row)
        })
    },

    delete: (id) => {
        return fetch('//localhost:8082/phonebook/delete/' + id)
    }
}
