import {SEARCH_UPDATE} from '../actions/Search'

let initialState = {
  searchPhrase: ''
}

export default (state = initialState, {type, payload}) => {
  switch (type) {
    case SEARCH_UPDATE: {
      return {...state, searchPhrase: payload.searchPhrase}
    }
    default: {
      return state
    }
  }
}
