let initialState = {
    rows: [],
    pending: false,
    error: false
}

export default (state = initialState, action) => {
    switch (action.type) {
        case 'FETCH_ROW': {
            return {...state, pending: true}
        }
        case 'CREATE_ROWS_SUCCESS': {
            return {
                ...state,
                rows: [...state.rows, {
                    _id: action.row._id,
                    name: action.row.name,
                    tel: action.row.tel,
                    email: action.row.email
                }],
                pending: false
            }
        }
        case 'READ_ROWS_SUCCESS': {
            return {...state, rows: action.fetchedRows, pending: false}
        }
        case 'DELETE_ROWS_SUCCESS': {
            return {...state, rows: state.rows.filter((val) => val._id !== action.id), pending: false}
        }
        case 'CREATE_ROWS_ERROR':
        case 'READ_ROWS_ERROR':
        case 'DELETE_ROWS_ERROR': {
            return {...state, rows: [], pending: false, error: true}
        }
        default: {
            return state
        }
    }
}
