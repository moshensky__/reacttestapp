let initialState = {
    pending: false,
    error: false
}

export default (state = initialState, action) => {
    switch (action.type) {
        case 'FETCH_EDIT_ROW': {
            return {...state, pending: true}
        }
        case 'UPDATE_ROWS_SUCCESS': {
            return state
        }
        case 'UPDATE_ROWS_ERROR': {
            return {...state, pending: false, error: true}
        }
        default: {
            return state
        }
    }
}
