import {combineReducers} from 'redux'
import {reducer as formReducer} from 'redux-form'
import searchReducer from './searchReducer'
import tableReducer from './tableReducer'
import editReducer from './editReducer'

export default combineReducers({
    search: searchReducer,
    table: tableReducer,
    edit: editReducer,
    form: formReducer
})
