import {connect} from 'react-redux'
import {Form} from './Form'
import {Validation} from "./Validation";

const mapStateToProps = () => ({
    form: 'form_edit',
    buttonTitle: 'Edit',
    validate: Validation
})

export const FormEdit = connect(
    mapStateToProps
)(Form)
