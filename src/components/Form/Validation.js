export const Validation = (values) => {
    let error = {};
    if (!values.name || values.name.length < 3) {
        error.name = 'Name is required and should be longer than 2 characters.'
    }
    if (!values.tel || !Number.isInteger(+values.tel)) {
        error.tel = 'Tel is required and should be as numeric.'
    }
    if (!values.email || !/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$/.test(values.email)) {
        error.email = 'Email is required and should be valid.'
    }
    return error
}