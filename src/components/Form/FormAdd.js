import {connect} from 'react-redux'
import {Form} from './Form'
import {Validation} from "./Validation";

const mapStateToProps = () => ({
    form: 'form_add',
    buttonTitle: 'Add',
    validate: Validation
})

export const FormAdd = connect(
    mapStateToProps
)(Form)
