import React from 'react'
import PropTypes from 'prop-types'
import {Field, reduxForm} from 'redux-form'
import style from './Form.css'

let Form = (props) =>
    <tr>
        <td>
            <form onSubmit={props.handleSubmit} id={props.form} />
        </td>
        <td>
            <Field name="name" component="input" type="text" className={style.field} />
        </td>
        <td>
            <Field name="tel" component="input" type="text" className={style.field} />
        </td>
        <td>
            <Field name="email" component="input" type="text" className={style.field} />
        </td>
        <td>
            <button className={style.add} type="submit" form={props.form} disabled={props.invalid}>{props.buttonTitle}</button>
        </td>
    </tr>

Form.propTypes = {
    onSubmit: PropTypes.func,
    form: PropTypes.string,
    buttonTitle: PropTypes.string
}

Form = reduxForm()(Form)

export {Form}