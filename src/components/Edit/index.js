import {connect} from 'react-redux'
import {EditComponent} from './Edit'

const mapStateToProps = (state, ownProps) => ({
    initialValues: state.table.rows.find((row) => row._id === ownProps.match.params._id),
    form: state.form.form_edit,
})

const mapDispatchToProps = (dispatch) => ({
    dispatch
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(EditComponent)
