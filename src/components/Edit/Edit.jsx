import React from 'react'
import PropTypes from 'prop-types'
import {Link, Redirect} from 'react-router-dom'

import {UPDATE_ROWS_HANDLER} from "../../actions/Edit";
import {FormEdit} from '../Form'

class EditComponent extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            redirect: false
        }
    }

    handleSubmit() {
        this.props.dispatch({type: UPDATE_ROWS_HANDLER, payload: {...this.props.form.values}})
        this.setState({
            redirect: true
        })
    }

    render() {
        if (this.state.redirect) {
            return (<Redirect to="/"/>)
        }
        return (
            <React.Fragment>
                <Link to="/">Back to list</Link>
                <h1>Edit contact</h1>
                <table>
                    <tbody>
                    <FormEdit
                        onSubmit={() => this.handleSubmit()}
                        initialValues={this.props.initialValues}
                    />
                    </tbody>
                </table>
            </React.Fragment>
        )
    }
}

EditComponent.propTypes = {
    dispatch: PropTypes.func,
    form: PropTypes.shape({
        values: PropTypes.shape({
            _id: PropTypes.string,
            name: PropTypes.string,
            tel: PropTypes.string,
            email: PropTypes.string
        })
    }),
    initialValues: PropTypes.shape({
        _id: PropTypes.string,
        name: PropTypes.string,
        tel: PropTypes.string,
        email: PropTypes.string
    })
}

export {EditComponent}