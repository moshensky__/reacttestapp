import React from 'react'
import classNames from 'classnames'
import PropTypes from 'prop-types'
import style from './Pending.css'

const Pending = ({active}) => {
    let classPending = classNames(style.pending, {[style.active]: active});
    return (
        <div className={classPending}>
            <img className={style.pending__image} src="/src/public/pending.gif" alt="Pending"/>
        </div>
    )
}

Pending.propTypes = {
    active: PropTypes.bool,
}

export {Pending}
