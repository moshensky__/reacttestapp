import React from 'react'
import PropTypes from 'prop-types'
import {Link} from "react-router-dom";
import './Row.css'

const Row = (props) =>
    <tr>
        <td>{props.index}</td>
        <td>{props.row.name}</td>
        <td>{props.row.tel}</td>
        <td>{props.row.email}</td>
        <td>
            <Link to={'/edit/' + props.row._id}>Edit</Link>
            <button className="delete" onClick={(e) => props.onDelete(e, props.index)}>Delete</button>
        </td>
    </tr>

Row.propTypes = {
    index: PropTypes.number,
    row: PropTypes.shape({
        _id: PropTypes.string,
        name: PropTypes.string,
        tel: PropTypes.string,
        email: PropTypes.string
    }),
    onDelete: PropTypes.func,
}

export {Row}