import {connect} from 'react-redux'
import {SearchComponent} from './Search'
import {updateSearch} from '../../actions/Search'

const mapStateToProps = (state) => ({
  searchPhrase: state.search.searchPhrase
})

const mapDispatchToProps = {
  updateSearch
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SearchComponent)
