import React from 'react'
import PropTypes from 'prop-types'
import style from './Search.css'

const SearchComponent = (props) =>
    <div className={style.search}>
        <span>Search: </span>
        <input type="search" className={style.searchField} value={props.searchPhrase} onChange={(e) => {
            props.updateSearch(e.target.value)
        }}/>
    </div>

SearchComponent.propTypes = {
    searchPhrase: PropTypes.string,
    updateSearch: PropTypes.func
}

export {SearchComponent}