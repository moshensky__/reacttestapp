import {connect} from 'react-redux'
import {reset as ResetForm, change as SetForm} from 'redux-form'
import TableComponent from './Table'
import {getSearchFilteredRows} from '../../selectors'

const mapStateToProps = (state) => ({
    rows: getSearchFilteredRows(state),
    searchPhrase: state.search.searchPhrase,
    pending: state.table.pending,
    form: state.form.form_add,
})

const mapDispatchToProps = (dispatch) => ({
    dispatch,
    ResetForm,
    SetForm
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(TableComponent)
