import React, {Component} from 'react'
import PropTypes from 'prop-types'

import {
    CREATE_ROWS_HANDLER,
    READ_ROWS_HANDLER,
    DELETE_ROWS_HANDLER
} from "../../actions/Table";
import {Row} from '../Row'
import {FormAdd} from '../Form'
import {Pending} from '../Pending'

class Table extends Component {
    /**
     * upload initial data
     */
    componentDidMount() {
        this.props.dispatch({type: READ_ROWS_HANDLER})
    }

    submitHandler() {
        if (this.props.form.values.name && this.props.form.values.tel && this.props.form.values.email) {
            this.props.dispatch({type: CREATE_ROWS_HANDLER, payload: {...this.props.form.values}})
            this.props.ResetForm('form_add')
        }
    }

    actionEdit(event, index) {
        if (index && this.props.rows[index]) {
            let currentRow = {...this.props.rows[index]}
            this.props.SetForm('form_add', 'index', +index)
            this.props.SetForm('form_add', 'name', currentRow.name)
            this.props.SetForm('form_add', 'tel', currentRow.tel)
            this.props.SetForm('form_add', 'email', currentRow.email)
        }
    }

    actionDelete(event, index) {
        this.props.dispatch({type: DELETE_ROWS_HANDLER, payload: this.props.rows[index]._id})
    }

    render() {
        return (
            <div className="wrapper">
                <table>
                    <thead>
                    <tr>
                        <td>#</td>
                        <td>Name</td>
                        <td>Tel</td>
                        <td>Email</td>
                        <td>Actions</td>
                    </tr>
                    </thead>
                    <tbody>
                    {this.props.rows.map((row, i) =>
                        <Row
                            key={i}
                            index={i}
                            row={row}
                            onEdit={(e, index) => {
                                this.actionEdit(e, index)
                            }}
                            onDelete={(e, index) => {
                                this.actionDelete(e, index)
                            }}
                            searchPhrase={this.props.searchPhrase}
                        ></Row>
                    )}
                    </tbody>
                    <tfoot>
                    <FormAdd onSubmit={() => this.submitHandler()}></FormAdd>
                    </tfoot>
                </table>
                <Pending active={this.props.pending}/>
            </div>
        )
    }
}

Table.propTypes = {
    rows: PropTypes.array,
    searchPhrase: PropTypes.string,
    pending: PropTypes.bool,
    form: PropTypes.shape({
        values: PropTypes.shape({
            index: PropTypes.number,
            name: PropTypes.string,
            tel: PropTypes.string,
            email: PropTypes.string,
            _id: PropTypes.string
        })
    }),
    dispatch: PropTypes.func,
    ResetForm: PropTypes.func,
    SetForm: PropTypes.func
}

export default Table
