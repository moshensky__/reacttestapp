import React from 'react'
import {Switch, Route} from 'react-router-dom'
import Search from './components/Search'
import Table from './components/Table'
import Edit from './components/Edit'
import './App.css'

export function App () {
    return (
        <div className="wrapper">
            <Switch>
                <Route exact path="/" component={Search} />
            </Switch>
            <Switch>
                <Route exact path="/" component={Table} />
                <Route exact path="/edit/:_id" component={Edit} />
            </Switch>
        </div>
    )
}
