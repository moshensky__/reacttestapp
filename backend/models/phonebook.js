const mongoose = require('mongoose');
// const autoIncrement = require('mongoose-auto-increment')

//Define a schema
const Schema = mongoose.Schema;

// autoIncrement.initialize(mongoose.connection);

const PhonebookSchema = new Schema({
    name: String,
    tel: String,
    email: String
});

// PhonebookSchema.plugin(autoIncrement.plugin, 'phonebook');
module.exports = mongoose.model('phonebook', PhonebookSchema);