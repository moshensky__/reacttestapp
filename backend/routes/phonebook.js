const express = require('express');
const router = express.Router();
const Phonebook = require('../models/phonebook');
const mongoose = require('mongoose');

/* CRUD api for phonebook */
router.post('/create', function (req, res) {
    let body = '';
    req.on('data', chunk => {
        body += chunk.toString(); // convert Buffer to string
    });
    req.on('end', () => {
        Phonebook.create(JSON.parse(body), function (err, row) {
            if (err) {
                res.status(500).send(JSON.stringify(err));
            }
            res.status(200).send(row._id);
        });
    });
});

router.get('/read', function (req, res) {
    Phonebook.find({}, '', function (err, phones) {
        if (err) {
            res.status(500).send(JSON.stringify(err));
        }
        res.status(200).send(JSON.stringify(phones));
    });
});

router.post('/update/:id', function (req, res) {
    let body = '';
    req.on('data', chunk => {
        body += chunk.toString(); // convert Buffer to string
    });
    req.on('end', () => {
        Phonebook.findByIdAndUpdate(req.params.id, JSON.parse(body), {new: true}, function (err, numAffected) {
            if (err) {
                res.status(500).send(JSON.stringify(err));
            }
            res.status(200).send(numAffected);
        });
    });
});

router.get('/delete/:id', function (req, res) {
    Phonebook.findByIdAndRemove(req.params.id, function (err) {
        if (err) {
            res.status(500).send(JSON.stringify(err));
        }
        res.status(200).send('ok');
    });
});

module.exports = router;
